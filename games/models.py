from django.db import models


class Categoria(models.Model):
    descricao = models.CharField(max_length=100)

    def __str__(self):
        return self.descricao


class Game(models.Model):
    nome = models.CharField(max_length=150)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT, related_name="game_categoria")


class ReportPermissions(models.Model):
    class Meta:
        abstract = True
        # mesmo que esteja no arquivo migrate de forma manual, bom referenciar aqui
        permissions = (
            ('view_report_a', "Ver relatorio A"),
            ('view_report_b', "Ver relatorio B")
        )