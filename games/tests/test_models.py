from django.test import TestCase

from games.models import Game, Categoria


class CategoriaTestCase(TestCase):

    def setUp(self):
        self.categoria = Categoria.objects.create(descricao="RPG")

    def tearDown(self):
        Categoria.objects.all().delete()

    def test_create_categoria(self):
        Categoria.objects.create(descricao='Luta')
        self.assertEqual(Categoria.objects.count(), 2)

    def test_update_categoria(self): 
        self.categoria.descricao = 'Puzzle'
        self.categoria.save()
        self.assertEqual(self.categoria.descricao, 'Puzzle')

    def test_delete_categoria(self):
        self.categoria.delete()
        self.assertEqual(Categoria.objects.count(), 0)


class GameTestCase(TestCase):

    def setUp(self):
        self.categoria = Categoria.objects.create(descricao="Witcher")
        self.categoria2 = Categoria.objects.create(descricao="Luta")
        self.game = Game.objects.create(nome="RPG",categoria=self.categoria)

    def tearDown(self):
        Game.objects.all().delete()
        Categoria.objects.all().delete()

    def test_create_game(self):
        Game.objects.create(nome='Persona',categoria=self.categoria)
        self.assertEqual(Game.objects.count(), 2)

    def test_update_game(self): 
        self.game.nome = 'Dark Souls'
        self.game.categoria = self.categoria2
        self.game.save()
        self.assertEqual(self.game.nome, 'Dark Souls')
        self.assertEqual(self.game.categoria, self.categoria2)

    def test_delete_game(self):
        self.game.delete()
        self.assertEqual(Game.objects.count(), 0)