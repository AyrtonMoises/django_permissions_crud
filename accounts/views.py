from django.shortcuts import render, redirect
from django.contrib import auth, messages


def login(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('home')

    if request.method == 'POST':
        username = request.POST['username']
        senha = request.POST['senha']

        if username == "" or senha == "":
            messages.error(request, "Campos username e senha não podem ficar em branco!")
            return redirect('login')
        user = auth.authenticate(request, username=username, password=senha)

        if user is not None:
            auth.login(request, user)
            proxima_pagina = request.POST.get('next')
            if proxima_pagina:
                return redirect(proxima_pagina)
            return redirect('home')
        else:
            messages.error(request, "username/senha estão incorretos")
            return render(request, 'login.html')

    return render(request, 'login.html')


def logout(request):
    """ Realiza logout do usuário """
    auth.logout(request)
    return redirect('login')
