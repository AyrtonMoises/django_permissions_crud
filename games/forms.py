from django import forms

from games.models import Game, Categoria


class CategoriaForm(forms.ModelForm):
    
    class Meta:
        model = Categoria
        fields = '__all__'


class GameForm(forms.ModelForm):
    
    class Meta:
        model = Game
        fields = '__all__'
