from django.test import TestCase
from django.contrib.auth import get_user_model



User = get_user_model()

class UserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='usuario',
            password='minhasenhasecreta',
            email='email@email.com'
        )

    def tearDown(self):
        User.objects.all().delete()

    def test_create_user(self):
        """ Criar um usuário """
        User.objects.create_user(
            username='usuario_teste',
            password='minhasenhasecreta',
            email='teste@email.com'
        )
        self.assertEqual(User.objects.count(), 2)

    def test_update_user(self): 
        """ Atualizar um usuário """  
        self.user.username = 'usuarionovo'
        self.user.email = 'novo@email.com'
        self.user.save()
        self.assertEqual(self.user.username, 'usuarionovo')
        self.assertEqual(self.user.email, 'novo@email.com')

    def test_update_password(self): 
        """ Atualiza senha um usuário """  
        self.user.set_password('new_password')
        self.assertTrue(self.user.check_password('new_password'))

        
    def test_delete_user(self):
        """ Deletar um usuário """
        self.user.delete()
        self.assertEqual(User.objects.count(), 0)
