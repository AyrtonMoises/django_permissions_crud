from django.urls import path
from .views import home, CategoriaCreateView, create_game, edit_game, delete_game, relatorio_A, relatorio_B


urlpatterns = [
    path('', home, name='home'),
    path('categoria/new', CategoriaCreateView.as_view(), name='categoria_new'),
    path('game/new', create_game, name='game_new'),
    path('game/edit/<int:pk>', edit_game, name='game_edit'),
    path('game/delete/<int:pk>', delete_game, name='game_delete'),
    path('relatorio-a', relatorio_A, name='relatorio_A'),
    path('relatorio-b', relatorio_B, name='relatorio_B'),
]

