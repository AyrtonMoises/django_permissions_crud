# Generated by Django 5.0.6 on 2024-05-21 17:45

from django.db import migrations


def create_custom_permissions(apps, schema_editor):
    Permission = apps.get_model('auth', 'Permission')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    # Create a generic content type for the permissions
    content_type, created = ContentType.objects.get_or_create(
        app_label='games', 
        model='reportpermissions' 
    )

    # Define custom permissions
    permissions = [
            ('view_report_a', "Ver relatorio A"),
            ('view_report_b', "Ver relatorio B")
    ]

    for codename, name in permissions:
        Permission.objects.create(
            codename=codename,
            name=name,
            content_type=content_type
        )


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0003_delete_report'),
    ]

    operations = [
        migrations.RunPython(create_custom_permissions),
    ]
