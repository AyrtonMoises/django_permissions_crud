from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponse

from games.forms import CategoriaForm, GameForm
from games.models import Game


@login_required
def home(request):
    games = Game.objects.select_related('categoria').all()
    data = {'games': games}
    return render(request, 'games.html', data)

@permission_required('games.add_game', raise_exception=True)
def create_game(request):
    form = GameForm(request.POST or None)

    if request.POST:
        if form.is_valid():
            form.save()
            return redirect('home')

    data = {'form': form}

    return render(request, 'game.html', data)

@permission_required('games.change_game', raise_exception=True)
def edit_game(request, pk):
    game = get_object_or_404(Game, id=pk)
    form = GameForm(request.POST or None, instance=game)

    if request.POST:
        if form.is_valid():
            form.save()
            return redirect('home')

    data = {'form': form}

    return render(request, 'game.html', data)

@permission_required('games.delete_game', raise_exception=True)
def delete_game(request, pk):
    game = get_object_or_404(Game, id=pk)
    game.delete()
    return redirect('home')

@permission_required('games.view_report_a', raise_exception=True)
def relatorio_A(request):
    return HttpResponse('Relatorio A')

@permission_required('games.view_report_b', raise_exception=True)
def relatorio_B(request):
    return HttpResponse('Relatorio B')


class CategoriaCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    template_name = 'categoria.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('home')
    success_message = "Categoria cadastrado com sucesso"
