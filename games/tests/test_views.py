from django.test import TestCase, Client
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import Permission, User

from games.models import Game, Categoria


class GameViewTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='usuario',
            password='senha123'
        )
        add_game = Permission.objects.get(codename='add_game')
        change_game = Permission.objects.get(codename='change_game')
        delete_game = Permission.objects.get(codename='delete_game')
        view_game = Permission.objects.get(codename='view_game')
        permissions_game = [add_game, change_game, delete_game, view_game]
        self.user.user_permissions.add(*permissions_game)

        self.user2 = User.objects.create_user(
            username='usuario2',
            password='senha123'
        )

        self.categoria = Categoria.objects.create(descricao="RPG")
        self.categoria2 = Categoria.objects.create(descricao="Luta")
        self.game = Game.objects.create(nome="Persona", categoria=self.categoria)
        
        self.client = Client()
        self.home_url = reverse('home')
        self.login_url = reverse('login')
        self.game_new_url = reverse('game_new')
        self.game_edit_url = reverse('game_edit', kwargs={'pk': self.game.id})
        self.game_delete_url = reverse('game_delete', kwargs={'pk': self.game.id})


    def tearDown(self):
        self.user.delete()
        Game.objects.all().delete()

    def test_home_not_autenticated(self):
        response = self.client.get(self.home_url)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.login_url + f"?next={self.home_url}")
        

    def test_home_games_ok(self):
        self.client.login(username=self.user.username, password='senha123')
        response = self.client.get(self.home_url)

        self.assertTrue(self.user.has_perm("games.view_game"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'games.html')

        games = response.context['games']
        self.assertEquals(games[0].nome, 'Persona')
        self.assertEquals(games[0].categoria.descricao, 'RPG')
        self.assertEqual(games.count(), 1)
        self.assertContains(response, 'Lista')

    def test_create_game_ok(self):
        self.client.login(username=self.user.username, password='senha123')
        response = self.client.get(self.game_new_url)

        self.assertTrue(self.user.has_perm("games.add_game"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'game.html')

        data = {'nome': 'Witcher', 'categoria': self.categoria.id}
        response = self.client.post(self.game_new_url, data)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.home_url)
        

    def test_create_game_sem_autenticacao(self):
        response = self.client.get(self.game_new_url)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.login_url + f"?next={self.game_new_url}")

    def test_create_game_sem_permissao(self):
        self.client.login(username=self.user2.username, password='senha123')
        response = self.client.get(self.game_new_url)

        self.assertEquals(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')

    def test_create_game_invalid(self):
        self.client.login(username=self.user.username, password='senha123')

        data = {'nome': '', 'categoria': 99999}
        response = self.client.post(self.game_new_url, data)
        self.assertTemplateUsed(response, 'game.html')
        self.assertFormError(response.context['form'], 'nome', 'Este campo é obrigatório.')
        self.assertFormError(response.context['form'], 'categoria', 'Faça uma escolha válida. Sua escolha não é uma das disponíveis.')

    def test_update_game_ok(self):
        self.client.login(username=self.user.username, password='senha123')
        response = self.client.get(self.game_edit_url)

        self.assertTrue(self.user.has_perm("games.change_game"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'game.html')

        data = {'nome': 'Witcher', 'categoria': self.categoria2.id}
        response = self.client.post(self.game_edit_url, data)

        self.game.refresh_from_db()

        self.assertEqual(self.game.nome, 'Witcher')
        self.assertEqual(self.game.categoria, self.categoria2)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.home_url)

    def test_update_game_sem_autenticacao(self):
        response = self.client.get(self.game_edit_url)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.login_url + f"?next={self.game_edit_url}")

    def test_update_game_sem_permissao(self):
        self.client.login(username=self.user2.username, password='senha123')
        response = self.client.get(self.game_edit_url)

        self.assertEquals(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')

    def test_update_game_invalid(self):
        self.client.login(username=self.user.username, password='senha123')

        data = {'nome': '', 'categoria': 99999}
        response = self.client.post(self.game_edit_url, data)
        self.assertTemplateUsed(response, 'game.html')
        self.assertFormError(response.context['form'], 'nome', 'Este campo é obrigatório.')
        self.assertFormError(response.context['form'], 'categoria', 'Faça uma escolha válida. Sua escolha não é uma das disponíveis.')

    def test_delete_game_ok(self):
        self.client.login(username=self.user.username, password='senha123')
        response = self.client.get(self.game_delete_url)

        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, self.home_url)

    def test_delete_game_not_exit(self):
        self.client.login(username=self.user.username, password='senha123')
        game_delete_url_not_existe = reverse('game_delete', kwargs={'pk': 100})

        response = self.client.get(game_delete_url_not_existe)

        self.assertEquals(response.status_code, 404)